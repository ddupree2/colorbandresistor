import unittest
from resistor import *

class TestCreateFormattedResistanceString(unittest.TestCase):
    
    def testWith4BandsIsValid(self):
      color = ['white', 'white', 'green', 'violet']
      string = createFormattedResistanceString(color)
      expected_string = '9900 Kohms +/- 0.1%'
      self.assertEqual(expected_string, string)
      
    def testWith5BandsIsValid(self):
      color = ['red', 'blue', 'orange', 'white', 'green']
      string = createFormattedResistanceString(color)
      expected_string = '263 Gohms +/- 0.5%'
      self.assertEqual(expected_string, string)
