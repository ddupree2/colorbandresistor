import unittest
from resistor import *

class TestDecodeSignigicantFigures(unittest.TestCase):
    
    def testIsValid(self):
      color = ['white', 'white', 'green', 'violet', 'orange']
      sigFig = decodeSignigicantFigures(color)
      expected_value = 995
      self.assertEqual(expected_value, sigFig)
      
    def testWith4BandsIsValid(self):
      color = ['orange', 'red', 'green', 'black']
      sigFig = decodeSignigicantFigures(color)
      expected_value = 32
      self.assertEqual(expected_value, sigFig)
      
    def testWithLeading0DeletesThe0On4BandIsValid(self):
      color = ['black', 'red', 'orange', 'white']
      sigFig = decodeSignigicantFigures(color)
      expected_value = 2
      self.assertEqual(expected_value, sigFig)
      
    def testWithLeading0DeletesThe0On5BandIsValid(self):
      color = ['black', 'black', 'white', 'gold', 'red']
      sigFig = decodeSignigicantFigures(color)
      expected_value = 9
      self.assertEqual(expected_value, sigFig)
