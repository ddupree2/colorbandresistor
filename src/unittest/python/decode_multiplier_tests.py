import unittest
from resistor import *

class TestDecodeMultiplier(unittest.TestCase):
    
    def testWithGreen4BandIsValid(self):
      color = ['white', 'white', 'green', 'violet']
      multiplier = decodeMultiplier(color)
      expected_value = ((100, 'K'))
      self.assertEqual(expected_value, multiplier)
      
    def testWithRed4BandIsValid(self):
      color = ['white', 'white', 'red', 'violet']
      multiplier = decodeMultiplier(color)
      expected_value = ((100, ''))
      self.assertEqual(expected_value, multiplier)
      
    def testWithWhite4BandIsValid(self):
      color = ['white', 'white', 'white', 'violet']
      multiplier = decodeMultiplier(color)
      expected_value = ((1, 'G'))
      self.assertEqual(expected_value, multiplier)
      
    def testWithViolet4BandIsValid(self):
      color = ['white', 'white', 'violet', 'violet']
      multiplier = decodeMultiplier(color)
      expected_value = ((10, 'M'))
      self.assertEqual(expected_value, multiplier)
      
    def testWithGreen5BandIsValid(self):
      color = ['white', 'white', 'green', 'green', 'red']
      multiplier = decodeMultiplier(color)
      expected_value = ((100, 'K'))
      self.assertEqual(expected_value, multiplier)
      
    def testWithRed5BandIsValid(self):
      color = ['orange', 'red', 'orange', 'red', 'brown']
      multiplier = decodeMultiplier(color)
      expected_value = ((100, ''))
      self.assertEqual(expected_value, multiplier)
      
    def testWithWhite5BandIsValid(self):
      color = ['red', 'white', 'orange', 'white', 'grey']
      multiplier = decodeMultiplier(color)
      expected_value = ((1, 'G'))
      self.assertEqual(expected_value, multiplier)
      
    def testWithViolet5BandIsValid(self):
      color = ['orange', 'white', 'green', 'violet', 'blue']
      multiplier = decodeMultiplier(color)
      expected_value = ((10, 'M'))
      self.assertEqual(expected_value, multiplier)
