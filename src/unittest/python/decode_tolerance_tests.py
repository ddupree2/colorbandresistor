import unittest
from resistor import *

class TestDecodeTolerance(unittest.TestCase):
    
    def testWithVioletBandIsValid(self):
      color = ['white', 'white', 'green', 'violet']
      tolerance = decodeTolerance(color)
      expected_value = 0.1
      self.assertEqual(expected_value, tolerance)
      
    def testWithRedBandIsValid(self):
      color = ['white', 'orange', 'green','red', 'red']
      tolerance = decodeTolerance(color)
      expected_value = 2.0
      self.assertEqual(expected_value, tolerance)      
      
      
