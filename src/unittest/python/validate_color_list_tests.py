import unittest
from resistor import *

class TestValidateColorList(unittest.TestCase):
    
    def testIsValid(self):
      color = ['white', 'white', 'green', 'violet']
      colors = validateColorList(color)
      expected_value = True
      self.assertEqual(expected_value, colors)
 
    def testWithInvalid4BandStringsIsInvalid(self):
      color = ['asdw', 'sadw', 'asdsw', 'asdw']
      with self.assertRaises(ValueError):
        validateColorList(color)
        
    def testWithInvalid4BandStringsIsInvalid(self):
      color = ['asdw', 'sadw', 'asdsw', 'asdw', 'sdaw']
      with self.assertRaises(ValueError):
        validateColorList(color)
        
    def testWithNumbersInListInvalid(self):
      color = ['red', 'red', '26', 'orange']
      with self.assertRaises(ValueError):
        validateColorList(color)       
      
    def testWith5BandsIsValid(self):
      color = ['white', 'white', 'green', 'violet', 'red']
      colors = validateColorList(color)
      expected_value = True
      self.assertEqual(expected_value, colors)
      
    def testNoBandsIsInvalid(self):
      color = []
      with self.assertRaises(ValueError):
        validateColorList(color)
        
    def testNoneAsTheListIsInvalid(self):
      color = [None]
      with self.assertRaises(ValueError):
        validateColorList(color)

    def testWithTooFewBandsIsInvalid(self):
      color = ['yellow', 'white', 'red']
      with self.assertRaises(ValueError):
        validateColorList(color)
      
    def testWithTooMantBandsIsInvalid(self):
      color = ['yellow', 'white', 'red', 'orange', 'white', 'yellow']
      with self.assertRaises(ValueError):
        validateColorList(color)
        
    def testWithNoListIsInvalid(self):
      color = {'hello'}
      with self.assertRaises(ValueError):
        validateColorList(color)
        
    def testWithNoneTypeIsInvalid(self):
      color = None
      with self.assertRaises(ValueError):
        validateColorList(color)
 
