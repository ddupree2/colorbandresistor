import unittest
import re
from resistor import *

class TestDecodeResistance(unittest.TestCase):
    
    def testWith4BandsIsValid(self):
      color = ['grey', 'violet', 'yellow', 'silver']
      dictionary = decodeResistance(color)
      expected_dict = {'value' : 870, 'units': 'Kohms', 'tolerance': 10.0, 'formatted': '870 Kohms +/- 10.0%'}
      self.assertEqual(expected_dict, dictionary)
    def testWith5BandsIsValid(self):
      color = ['red', 'blue', 'green', 'gold', 'violet']
      dictionary = decodeResistance(color)
      expected_dict = {'value' : 26.5, 'units': 'ohms', 'tolerance': 0.1, 'formatted': '26.5 ohms +/- 0.1%'}
      self.assertEqual(expected_dict, dictionary)
