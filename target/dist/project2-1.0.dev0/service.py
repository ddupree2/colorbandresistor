import http.server
import urllib.request
from resistor import *
from urllib.parse import urlparse, parse_qs, parse_qsl


class Service(http.server.BaseHTTPRequestHandler):
    def do_GET(self):    
        try:
          path = self.path
          self.log_message("resource: %s", path)
          
          if not (path[0:7] == "/decode"): self.send_error(404, 'Resource needs to be /decode')
          
          parsed = urlparse(path)
          query = parsed.query
          decoded = parse_qs(str(query))
          
          colorList = []
          for band in decoded:
            colorList = colorList + decoded[band]
            
          result = decodeResistance(colorList) 
          
          body = str(result['formatted'])
       
          self.send_response(200)
          self.send_header('Content-Type', 'text/html')
          self.end_headers()
          self.wfile.write(bytes(body, 'UTF-8'))
        except Exception as e:
          self.send_error(400, str(e))
        
        
print('Webserver running on port 8000')
daemon = http.server.HTTPServer(('', 8000), Service)
daemon.serve_forever()


