import re
toleranceDict = {'brown': 1, 'red': 2, 'yellow': 'none', 'green': 0.5, 'blue': 0.25, 'violet': 0.1, 'grey': 0.05,'gold': 5, 'silver': 10}

multiplyDict = {'black': 1, 'brown': 10, 'red': 100, 'orange' : 1, 'yellow': 10, 'green': 100, 'blue': 1, 'violet': 10, 'grey': 100, 'white': 1, 'gold': 0.1, 'silver': 0.01}

sigFigsDict = {'black': '0', 'brown': '1', 'red': '2', 'orange' : '3', 'yellow': '4', 'green': '5', 'blue': '6', 'violet': '7', 'grey': '8', 'white': '9'}

sigRegex = re.compile(r'(black|brown|red|orange|yellow|green|blue|violet|grey|white)')
tolRegex = re.compile(r'(brown|red|green|blue|violet|grey|gold|silver)')
multRegex = re.compile(r'(black|brown|red|orange|yellow|green|blue|violet|grey|white|gold|silver)')

kMultiPrefixRegex = re.compile(r'(orange|yellow|green)')
mMultiPrefixRegex = re.compile(r'(blue|violet|grey)')
gMultiPrefixRegex = re.compile(r'white')


def validateColorList(colors):
  if isinstance(colors, list):
    if len(colors) > 5 or len(colors) <= 3:
      raise ValueError('Not the correct number of bands.')
    elif len(colors) == 5:
      if sigRegex.fullmatch(colors[0]) and sigRegex.fullmatch(colors[1]) and sigRegex.fullmatch(colors[2]) and multRegex.fullmatch(colors[3]) and tolRegex.fullmatch(colors[4]):
        return True
      else:
        raise ValueError('incorrect band at location')
    elif len(colors) == 4:
      if sigRegex.fullmatch(colors[0]) and sigRegex.fullmatch(colors[1])and multRegex.fullmatch(colors[2]) and tolRegex.fullmatch(colors[3]):
        return True
      else:
        raise ValueError('incorrect band at location')
  else:
    raise ValueError('That is not a list.')
      

def decodeTolerance(colors):

  validateColorList(colors)
  
  if len(colors) == 5:
    tolerance = toleranceDict.get(colors[4])
    return float(tolerance)
  elif len(colors) == 4:
    tolerance = toleranceDict.get(colors[3])  
    return float(tolerance)
    
def decodeMultiplier(colors):
  validateColorList(colors)
  
  if len(colors) == 5:
    multiplicty = multiplyDict.get(colors[3])
    prefix = ''
    
    if kMultiPrefixRegex.fullmatch(colors[3]):
      prefix = 'K'
    elif mMultiPrefixRegex.fullmatch(colors[3]):
      prefix = 'M'
    elif gMultiPrefixRegex.fullmatch(colors[3]):
      prefix = 'G'
    else: 
      prefix = ''
      
    final = (multiplicty, prefix)
    return final
  
  if len(colors) == 4:
    multiplicty = multiplyDict.get(colors[2])
    prefix = ''
    
    if kMultiPrefixRegex.fullmatch(colors[2]):
      prefix = 'K'
    elif mMultiPrefixRegex.fullmatch(colors[2]):
      prefix = 'M'
    elif gMultiPrefixRegex.fullmatch(colors[2]):
      prefix = 'G'
    else: 
      prefix = ''
      
    final = (multiplicty, prefix)
    return final

def decodeSignigicantFigures(colors):
  if len(colors) == 5:
    counter = 0
    sigFig = ''
    while counter < 3:
       sigFig = sigFig + sigFigsDict.get(colors[counter])
       counter += 1
    
    return int(sigFig)
  
  if len(colors) == 4:
    counter = 0
    sigFig = ''
    while counter < 2:
       sigFig = sigFig + sigFigsDict.get(colors[counter])
       counter += 1
    
    return int(sigFig)
    
def createFormattedResistanceString(colors):
  sigFig = decodeSignigicantFigures(colors)
  tolerance = decodeTolerance(colors)
  multiplier = decodeMultiplier(colors)
  
  formattedString = str(sigFig * multiplier[0]) + ' ' + str(multiplier[1]) + 'ohms +/- ' + str(tolerance) + '%'
  
  return formattedString
  
def decodeResistance(colors):
  
  validateColorList(colors)

  sigFig = decodeSignigicantFigures(colors)
  tolerance = decodeTolerance(colors)
  multiplier = decodeMultiplier(colors)
  
  value = sigFig * multiplier[0]
  units = str(multiplier[1]) + 'ohms'
  tolerance = tolerance
  formatted = createFormattedResistanceString(colors) 
    
  fullDict = {'value' : value, 'units': units, 'tolerance': tolerance, 'formatted': formatted}
  
  return fullDict        
      
      
