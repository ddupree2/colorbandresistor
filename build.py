#@Author: Darrius Dupree


from pybuilder.core import init, use_plugin, Author, task
from subprocess import call
from os import path


use_plugin("python.core")
use_plugin("python.unittest")
#use_plugin("python.coverage")
use_plugin("python.install_dependencies")
use_plugin("python.distutils")

default_task = "publish"

target_dir = path.join('target', 'dist', 'project2-1.0.dev0')

@init
def initialize(project):
  project.build_depends_on('mockito')
  project.authors = [Author('Darrius Dupree', 'ddupree2@my.westga.edu')]
  
  

@task
def run_http_server(project):
  call (['python', path.join(target_dir, 'service.py')])
